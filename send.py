import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import random
import hashlib


'''
    Secret "Secret Santa"
'''

def main():
    draw_name = input("Which name shall we give to this draw?")
    draw_year = input("What year are we? ")
    print("Starting...")

    # Read the participants
    f = open("participants.txt","r")
    temp = [l for l in f]
    people = [tuple(l.replace("\n","").split("/")) for l in temp]
    f.close()

    # Shuffling the bag
    print('Shuffling...')
    while True:
        senders, receivers = shuffle_draw(people)
        if not verify_repeated(senders, receivers):
            break

    # Saving results
    f = open("results" + draw_name.lower() + draw_year + ".txt","w")
    for i in range(len(senders)):
        #print(str(senders[i]) + "=>" + str(receivers[i]))
        string_final = senders[i][0] + receivers[i][0]
        hash_object = hashlib.md5(string_final.encode())
        f.write(hash_object.hexdigest() + "\n")
    f.close()


    print("Sending...")
    send_emails(draw_name + draw_year, senders,receivers)
    
    print("Done...")


def shuffle_draw(people):
    senders = people.copy()
    receivers = people.copy()

    random.shuffle(senders)
    random.shuffle(receivers)

    return (senders, receivers)

# Make sure no one sends a gift to itself
def verify_repeated(senders,receivers):
    for i in range(len(senders)):
        if senders[i][0] == receivers[i][0]:
            return True
    return False

def send_emails(draw, senders,receivers):
    f = open("config.txt","r")
    dados = [l.replace("\n","") for l in f]
    s = smtplib.SMTP(host='smtp.gmail.com', port=587)
    s.ehlo()
    s.starttls()
    s.login(dados[0], dados[1])

    # Para cada pessoa enviar mail
    for i in range(len(senders)):
        msg = MIMEMultipart()       # creaating message

        message = "Hello " + senders[i][0] + "! In the " + draw + " draw you got: " + receivers[i][0]
        # Prints out the message body for our sake
        #print(message)

        # setup the parameters of the message
        msg['From']=dados[0]
        msg['To']=senders[i][1]
        msg['Subject']="Draw " + draw

        # add in the message body
        msg.attach(MIMEText(message, 'plain'))
        
        # send the message via the server set up earlier.
        s.send_message(msg)
        del msg
        
    # Terminar ligação
    s.quit()


if __name__ == "__main__":
    main()
