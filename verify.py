import hashlib

def main():
    sorteio = input("Que sorteio foi (Reis/Natal)? ")
    ano = input("Que ano foi? ")

    pessoa1 = input("Quem és? ")
    pessoa2 = input("Quem achas que te calhou? ")

    f = open("resultados" + sorteio.lower() + ano + ".txt","r")
    resultados = [l.replace("\n","") for l in f]
    f.close()
    
    string_final = pessoa1 + pessoa2
    hash_object = hashlib.md5(string_final.encode())

    for r in resultados:
        if hash_object.hexdigest() == r:
            print("É mesmo isso, da próxima vez que te esqueceres levas uma velinha!")
            return
    print("Não é par...")


if __name__ == "__main__":
    main()
